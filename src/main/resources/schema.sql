drop table if exists STUDENTS;
create table STUDENTS
(
    ID        INTEGER auto_increment,
    NAME      CHARACTER VARYING(100) not null,
    HEIGHT    DOUBLE PRECISION       not null,
    BIRTHDATE DATE                   not null,
    constraint "STUDENTS_pk"
        primary key (ID)
);
