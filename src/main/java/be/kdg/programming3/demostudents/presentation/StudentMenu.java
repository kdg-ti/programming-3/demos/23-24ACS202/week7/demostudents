package be.kdg.programming3.demostudents.presentation;

import be.kdg.programming3.demostudents.domain.Student;
import be.kdg.programming3.demostudents.exceptions.StudentException;
import be.kdg.programming3.demostudents.repository.StudentRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

@Component
public class StudentMenu {
    private StudentRepository studentRepository;
    private Scanner scanner = new Scanner(System.in);

    public StudentMenu(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void show() {
        while (true) {
            System.out.print("""
                    Welcome to the Student Management System
                    ========================================
                    1) List students
                    2) Add student
                    3) Update student
                    4) Delete student
                    Make a choice:""");
            int choice = scanner.nextInt();
            scanner.nextLine();//remove <enter> from keyboardbuffer...
            switch (choice) {
                case 1 -> listStudents();
                case 2 -> createStudent();
            }
        }
    }

    private void createStudent() {
        System.out.println("Adding a student");
        System.out.println("================");
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Height:");
        double height = scanner.nextDouble();
        System.out.println("Birthdate:");
        System.out.print("year:");
        int year = scanner.nextInt();
        System.out.print("month:");
        int month = scanner.nextInt();
        System.out.print("day:");
        int day = scanner.nextInt();
        Student newStudent = new Student(name, height, LocalDate.of(year, month, day));
        try {
            Student returnedStudent = studentRepository.createStudent(newStudent);
            System.out.println("Created student with id " + returnedStudent.getId());
        } catch (StudentException e) {
            System.out.println(e.getMessage());
        }
    }

    private void listStudents() {
        System.out.println("Listing all students");
        System.out.println("====================");
        try {
            List<Student> studentList = studentRepository.findAll();
            studentList.forEach(System.out::println);
        } catch (StudentException studentException) {
            System.out.println(studentException.getMessage());
        }
    }
}
