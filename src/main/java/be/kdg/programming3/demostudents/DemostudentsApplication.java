package be.kdg.programming3.demostudents;

import be.kdg.programming3.demostudents.presentation.StudentMenu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemostudentsApplication {

    public static void main(String[] args) {
        ApplicationContext context =
                SpringApplication.run(DemostudentsApplication.class, args);
        context.getBean(StudentMenu.class).show();
    }

}
