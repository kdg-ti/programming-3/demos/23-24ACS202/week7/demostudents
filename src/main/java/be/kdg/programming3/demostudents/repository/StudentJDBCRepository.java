package be.kdg.programming3.demostudents.repository;

import be.kdg.programming3.demostudents.domain.Student;
import be.kdg.programming3.demostudents.exceptions.StudentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//@Repository
public class StudentJDBCRepository implements StudentRepository {
    @Value("${spring.datasource.url}")
    private String dbURL;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    @Override
    public List<Student> findAll() {
        List<Student> studentList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(dbURL,
                username, password)) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS")){
                    while (resultSet.next()) {
                        int id = resultSet.getInt("ID");
                        String studentname = resultSet.getString("NAME");
                        double height = resultSet.getDouble("HEIGHT");
                        LocalDate date = resultSet.getDate("BIRTHDATE").toLocalDate();
                        Student student = new Student(id, studentname, height, date);
                        studentList.add(student);
                    }
                }
            }
        } catch (SQLException e) {
            //we have to write this catch because SQLException is a checked exception!
            //maybe also log the message!
            throw new StudentException("Unable to connect to the database", e);
        }
        return studentList;
    }

    @Override
    public Student createStudent(Student student) {
        try (Connection connection = DriverManager.getConnection(dbURL,
                username, password)) {
            try (PreparedStatement statement
                         = connection.prepareStatement("INSERT INTO STUDENTS(NAME, HEIGHT, BIRTHDATE) " +
                    "VALUES (?, ?, ?)",Statement.RETURN_GENERATED_KEYS)){
                statement.setString(1, student.getName());
                statement.setDouble(2, student.getHeight());
                statement.setDate(3, Date.valueOf(student.getBirthdate()));
                int result = statement.executeUpdate();
                if (result != 0) {
                    try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            student.setId(generatedKeys.getInt(1));
                        }
                        else {
                            throw new SQLException("Creating student failed, no ID obtained.");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            //we have to write this catch because SQLException is a checked exception!
            //maybe also log the message!
            throw new StudentException("Problem creating the student", e);
        }
        return student;
    }

    @Override
    public void updateStudent(Student student) {

    }

    @Override
    public void deleteStudent(int id) {

    }
}
