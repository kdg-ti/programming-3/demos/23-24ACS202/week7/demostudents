package be.kdg.programming3.demostudents.repository;

import be.kdg.programming3.demostudents.domain.Student;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class StudentJDBCTemplateRepository implements StudentRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert studentInsert;

    public StudentJDBCTemplateRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.studentInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("STUDENTS")
                .usingGeneratedKeyColumns("ID");

    }

    @Override
    public List<Student> findAll() {
        return jdbcTemplate.query("SELECT * FROM STUDENTS",
                this::mapRow
        );
    }

    private Student mapRow(ResultSet rs, int i) throws SQLException {
        return new Student(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDouble("HEIGHT"),
                rs.getDate("BIRTHDATE").toLocalDate());
    }

    @Override
    public Student createStudent(Student student) {
        int studentId = studentInsert.executeAndReturnKey(Map.of(
                "NAME", student.getName(),
                "HEIGHT", student.getHeight(),
                "BIRTHDATE", Date.valueOf(student.getBirthdate())
        )).intValue();
        student.setId(studentId);
        return student;
    }

    @Override
    public void updateStudent(Student student) {

    }

    @Override
    public void deleteStudent(int id) {

    }
}
