package be.kdg.programming3.demostudents.repository;

import be.kdg.programming3.demostudents.domain.Student;

import java.util.List;

//higher layers talk to this interface...
public interface StudentRepository {
    List<Student> findAll();
    Student createStudent(Student student);//returns student with id
    void updateStudent(Student student);
    void deleteStudent(int id);
}
