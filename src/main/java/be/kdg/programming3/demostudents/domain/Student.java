package be.kdg.programming3.demostudents.domain;

import java.time.LocalDate;

public class Student {
    private int id = -1;
    private String name;
    private double height;
    private LocalDate birthdate;

    public Student(int id, String name, double height, LocalDate birthdate) {
        this.id = id;
        this.name = name;
        this.height = height;
        this.birthdate = birthdate;
    }

    public Student(String name, double height, LocalDate birthdate) {
        this.name = name;
        this.height = height;
        this.birthdate = birthdate;
    }

    //id needs a setter so the repo can add the id afterwards...
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public double getHeight() {
        return height;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", birthdate=" + birthdate +
                '}';
    }
}
